; My configuration for ispell

(setenv "DICPATH" "/usr/share/hunspell/")  ; Set $DICPATH to correct PATH for hunspell.
(setq ispell-program-name "hunspell")  ; Set hunspell as spelling checker.
