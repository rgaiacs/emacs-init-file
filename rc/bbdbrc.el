;;;; BBDB
(setq bbdb-file "~/.bbdb")
(require 'bbdb-loaddefs)
(require 'bbdb)
(bbdb-initialize 'gnus 'message)
(bbdb-mua-auto-update-init 'gnus 'message)

;; Sometimes people have work and personal e-mail addresses.
(setq bbdb-complete-name-allow-cycling t)

;; size of the bbdb popup
(setq bbdb-pop-up-window-size 10)

;; What do we do when invoking bbdb interactively
(setq bbdb-mua-update-interactive-p '(query . create))

;; Make sure we look at every address in a message and not only the
;; first one
(setq bbdb-message-all-addresses t)

;; use ; on a message to invoke bbdb interactively
(add-hook
   'gnus-summary-mode-hook
    (lambda ()
         (define-key gnus-summary-mode-map (kbd ";") 'bbdb-mua-edit-field)
	    ))

; Set correct phone number format
(setq bbdb-north-american-phone-numbers-p nil)  ; For BBDB < 3
(setq bbdb-phone-style nil)  ; For BBDB >= 3
