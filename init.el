;;;; Set up libraries
(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "http://melpa-stable.milkbox.net/packages/") t)
(package-initialize)

;;;; Configuration files
(add-to-list 'load-path "/home/raniere/.emacs.d/rc")
(load "bbdbrc")
(load "ispellrc")
(load "latexrc")
(load "orgmoderc")
(load "pythonrc")

;;;; Customization file
(setq custom-file "~/.emacs-custom.el") ; We don't want to version control this!
(load custom-file)

;;;; Global definitions
(add-hook 'text-mode-hook 'flyspell-mode) ; Enable Flyspell for text-based major modes
(column-number-mode t)			; Show column number in the mode line.
(line-number-mode t)			; Show line number in the mode line.
(setq-default major-mode 'text-mode)    ; Set default mode to plain text.
(linum-mode t)                         ; Enable line number on the left.
(auto-save-mode t)                     ; Enable auto save.
(visual-line-mode t)                   ; Enable line wrap.

;;;; GUI
(tool-bar-mode -1)			; Remove tool bar

;;;; Web browser
(setq browse-url-browser-function 'browse-url-generic browse-url-generic-program "chromium")

;;;; User
(setq user-mail-address "raniere@rgaiacs.com") ; Set email address

;;;; Typing
(add-hook 'text-mode-hook 'turn-on-auto-fill) ; Lines are broken automatically when they become too wide.
(setq-default fill-column 79)		; Set the number to the number of columns to use.
(setq-default indent-tabs-mode nil)	; No tabs
